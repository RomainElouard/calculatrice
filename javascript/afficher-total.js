(function () {
    //test si calculette installée dans la page
    if (!('calculette' in window)) {
        calculette.logger.error("AfficheurTotal","Problème pas de calculette");
        return; //sortie de la fonction
    }

    //type(class) AfficheurTotal
    function AfficheurTotal(idZone){
        this.idZone = idZone;
        
        this.init = function(){
            //mise en place valeur par défaut
            calculette.logger.log("AfficheurTotal",'init de la vue affiche total');
            this.setValue();
        };

        this.setValue = function(){
            $("#"+this.idZone).text(calculette.model.total);
        };  
        
        this.refreshView = function(){
            this.setValue();
        };

    }

    var afficheur = new AfficheurTotal('zoneTotal');

    calculette.aboInitApp(afficheur); 

})();