(function () {
    //test si calculette installée dans la page
    if (!('calculette' in window)) {
        calculette.logger.error("ServiceControler","Problème pas de calculette");
        return; //sortie de la fonction
    }

    function ServiceControler(){
        this.init = function(){

        };

        //les fonctions exportées du controler
        this.plus = function(valeur){
            calculette.model.total += valeur;
            calculette.model.addOperationPlus(valeur);
            calculette.refreshAllViews();
            calculette.saveModel();
        };

        this.moins = function(valeur){
            calculette.model.total -= valeur;
            calculette.model.addOperationMoins(valeur);
            calculette.refreshAllViews();
            calculette.saveModel();
        };

        this.reset = function(){
            calculette.model.total = 0;
            calculette.model.addOperationReset();
            calculette.refreshAllViews();
            calculette.saveModel(); 
        };

        this.addChiffre = function(chiffre){
            if (calculette.model.saisieVal.length >= 8){
                calculette.model.messageNotification = "Max chiffres !!!!";
                calculette.refreshAllViews();
                throw "max 8";
            }
            calculette.model.saisieVal += chiffre;
            calculette.refreshAllViews();
        }

        this.processTotal4Log = function(){
            calculette.model.processTotal4Log();
        }

    }

    var serviceControler = new ServiceControler();
    calculette.aboInitApp(serviceControler); 
    calculette.controler = serviceControler;



})();