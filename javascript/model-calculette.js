(function () {
    //test si calculette installée dans la page
    if (!('calculette' in window)) {
        calculette.logger.error("ModelCalculette","Problème pas de calculette");
        return; //sortie de la fonction
    }

    //type operation debut
    function Operation(valeurOperation){
        this.typeOperation = 'P';
        this.valeurOperation = valeurOperation;
        this.dateOperation = new Date();
        this.processTotal = function(current){
            return current+= this.valeurOperation;
        }
    }
    //type operation fin

    function OperationMoins( valeurOperation){
        this.typeOperation = 'M';
        this.valeurOperation = valeurOperation;
        this.dateOperation = new Date();
        this.processTotal = function(current){
            return current -= this.valeurOperation;
        }
    }

    function OperationReset(){
        this.typeOperation = 'R';
        this.valeurOperation = 0;
        this.dateOperation = new Date();
        this.processTotal = function(current){
            return 0;
        }
    }


    function ModelCalculette(){
        this.messageNotification = "";
        this.saisieVal = ""; //
        this.total = 0;
        this.journalOperation = [];

        this.init = function(){
            //rechargement du storage
            this.loadStorage();
        };

        this.saveStorage = function(){
            //sauvegarde du total dans le localStorage
            localStorage["total"] = this.total;
            //localStorage.total = this.total;
            calculette.logger.log("ModelCalculette",JSON.stringify(this.journalOperation));
        };

        this.loadStorage = function(){
            //test si total existe dans localStorage
            //obligatoire pour permière exécution sur le navigateur
            if ('total' in localStorage){
                // correction 'problème concatenation valeur après loadstorage'
                //this.total = localStorage.total;
                this.total = parseInt(localStorage.total,10);
            }
        };

        //fonction ajout operation
        this.addOperationPlus = function(valeur){
            var newOp = new Operation(valeur); //creation instance Operation
            this.journalOperation.push(newOp);
        };

        this.addOperationMoins = function(valeur){
            var newOp = new OperationMoins(valeur); //creation instance Operation
            this.journalOperation.push(newOp);
        };

        this.addOperationReset = function(){
            var newOp = new OperationReset(); //creation instance Operation
            this.journalOperation.push(newOp);
        };

        this.processTotal4Log = function(){
            var totalWrk = 0;

            for (var cpt = 0 ; cpt < this.journalOperation.length ; cpt++){
                totalWrk = this.journalOperation[cpt].processTotal(totalWrk);
            }
            calculette.logger.log("ModelCalculette","Ret total wrk = " + totalWrk);    
        }





    }

    //creation de l'instance du model
    var model = new ModelCalculette();   
    calculette.aboInitApp(model); 
    calculette.model = model; //mise en place du model

})();