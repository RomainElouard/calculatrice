(function () {
    //test si calculette installée dans la page
    if (!('calculette' in window)) {
        calculette.logger.error("AfficheurTotalHexa","Problème pas de calculette");
        return; //sortie de la fonction
    }

    //type(class) AfficheurTotal
    function AfficheurTotalHexa(idZone){
        this.idZone = idZone;
        
        this.init = function(){
            //mise en place valeur par défaut
            calculette.logger.log("AfficheurTotalHexa",'init de la vue affiche total hexa');
            this.setValue();
        };

        this.setValue = function(){
            var conv = 'Hexa: '+calculette.model.total.toString(16);
            $("#"+this.idZone).text(conv);
        };  
        
        this.refreshView = function(){
            this.setValue();
        };

    }

    var afficheur = new AfficheurTotalHexa('zoneTotalHexa');

    calculette.aboInitApp(afficheur); 

})();