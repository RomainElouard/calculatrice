(function () {
    //test si calculette installée dans la page
    if (!('calculette' in window)) {
        calculette.logger.error("AfficheurMessageNotification","Problème pas de calculette");
        return; //sortie de la fonction
    }

    //type(class) AfficheurMessageNotification
    function AfficheurMessageNotification(idZone) {
        this.idZone = idZone;

        this.init = function () {
            //mise en place valeur par défaut
            calculette.logger.log("AfficheurMessageNotification",'init de la vue affiche message notification');
            this.setValue();
        };

        this.setValue = function () {
            $("#" + this.idZone).removeClass("alert alert-danger");
            if (calculette.model.messageNotification){
                $("#" + this.idZone).addClass("alert alert-danger");
            }
            $("#" + this.idZone).text(calculette.model.messageNotification);
        };

        this.refreshView = function () {
            this.setValue();
            if (calculette.model.messageNotification.length != 0) {
                setTimeout(function () {
                    calculette.model.messageNotification = "";
                    calculette.refreshAllViews();
                }, 2000);
            }
        };


    }

    var afficheurMessage = new AfficheurMessageNotification('zoneMessage');

    calculette.aboInitApp(afficheurMessage);

})();