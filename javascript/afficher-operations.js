(function () {
   //test si calculette installée dans la page
   if (!('calculette' in window)) {
      calculette.logger.error("AfficheurJournal", "Problème pas de calculette");
      return; //sortie de la fonction
   }

   //type(class) AfficheurJournal
   function AfficheurJournal(idZone) {
      this.idZone = idZone;
      this.racineListe = null; //zone liste dans le div dans l'HTML

      this.init = function () {
         //mise en place valeur par défaut
         calculette.logger.log("AfficheurJournal", 'init de la vue afficheur Journal');
         //premaration de la zone
         this.prepareZoneAffichage(),
            this.setValues();
      };

      this.prepareZoneAffichage = function () {
         this.racineListe = $('<ul>');
         $('#' + this.idZone).append(this.racineListe);
      };


      this.setValues = function () {
         //reset zone ul
         this.racineListe.empty();
         //iteration journal des opérations
         for (var cpt = 0; cpt < calculette.model.journalOperation.length; cpt++) {
            //recuperation instance operation dans le tableau
            var opIteration = calculette.model.journalOperation[cpt];
            //creation de la ligne d'affichage
            var str = "Valeur : " + opIteration.valeurOperation + "  --- Type: " +
               opIteration.typeOperation;
            //creation nouvelle ligne 'li'
            var newLi = $('<li data-toggle="modal" data-target="#exampleModal" data-heure="' + opIteration.dateOperation.getHours() + ':' + opIteration.dateOperation.getMinutes() + '">');
            //mise en place du texte de la ligne
            newLi.text(str);
            //ajout du LI dans le UL
            this.racineListe.append(newLi);
            newLi
            newLi.on("click", function () {
               var recup = $(this);
               var heure = $(this).attr('data-heure');
               $(".contenu").text("");
               $(".contenu").append(recup.html());
               $(".contenu").append(" à ");
               $(".contenu").append(heure);
            })
         }

      };

      this.refreshView = function () {
         this.setValues();
      };

   }

   var afficheur = new AfficheurJournal('zoneOperations');

   calculette.aboInitApp(afficheur);

})();