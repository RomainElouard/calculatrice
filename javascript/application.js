(function(){

    //gestion du logging
    function LoggerApp(){

        this.formatMessage = function(severity , emetteur , message){
            return "["+severity.toUpperCase()+"]["+new Date().toISOString()+"]["+emetteur.toUpperCase()+"]>>>"+message
        };

        this.log = function(emetteur , message){
            if (arguments.length == 0){
                return;
            }
            if (arguments.length == 1){
                message = emetteur;
                emetteur = "global";
            }
            console.log(this.formatMessage('INFO',emetteur ,message))
        };

        this.error = function(emeteur , message){
            if (arguments.length == 0){
                return;
            }
            if (arguments.length == 1){
                message = emetteur;
                emetteur = "global";
            }
            console.error(this.formatMessage('ERROR',emetteur ,message))
        };

    }


    //moteur applicatif
    var moteurApp = {
        controler: null, // reference du controler de l'application
        model: null, //reference du model de l'application
        listeDesElementsAbonnes: [],
        logger: new LoggerApp(),
        main : function(){ //point entree de l'application
        calculette.logger.log("Application","Debut application");
            //iteration sur le tableau listeDesElementsAbonnes
            for (var cpt = 0 ; cpt < moteurApp.listeDesElementsAbonnes.length;cpt++){
                var tmpObj = moteurApp.listeDesElementsAbonnes[cpt];
                if ('init' in tmpObj){
                    tmpObj.init(); //appel init     
                }else{
                    calculette.logger.error("Application","Init Objet incompatible");
                }
            }
        },
        aboInitApp : function(elementAAbonner){
            //insertion de l'element dans le tableau
            moteurApp.listeDesElementsAbonnes.push(elementAAbonner);
        },
        refreshAllViews : function(){
            for (var cpt = 0 ; cpt < moteurApp.listeDesElementsAbonnes.length;cpt++){
                var tmpObj = moteurApp.listeDesElementsAbonnes[cpt];
                if ('refreshView' in tmpObj){
                    tmpObj.refreshView(); //appel init     
                }
            }
        },
        saveModel : function(){
            moteurApp.model.saveStorage(); //sauvegarde dans le storage
        }

    }; //fin du moteur applicatif

    //installation du moteur en global (dans window)
    window.calculette = moteurApp;

    //abonnement de main à l'évènement ready du document
    $(document).ready(moteurApp.main);


})();

