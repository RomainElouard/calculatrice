(function () {
    //test si calculette installée dans la page
    if (!('calculette' in window)) {
        calculette.logger.error("AfficheurSaisie","Problème pas de calculette");
        return; //sortie de la fonction
    }

    //type(class) AfficheurSaisie
    function AfficheurSaisie(idZone){
        this.idZone = idZone;
        
        this.init = function(){
            //mise en place valeur par défaut
            calculette.logger.log("AfficheurSaisie",'init de la vue affiche saisie');
            this.setValue();
        };

        this.setValue = function(){
            $("#"+this.idZone).text(calculette.model.saisieVal);
        };  
        
        this.refreshView = function(){
            this.setValue();
        };

    }

    var afficheur = new AfficheurSaisie('zoneSaisie');

    calculette.aboInitApp(afficheur); 

})();