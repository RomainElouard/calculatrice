(function () {
    //test si calculette installée dans la page
    if (!('calculette' in window)) {
        calculette.logger.error("ClavierChiffres","Problème pas de calculette");
        return; //sortie de la fonction
    }

    //creation du type (class) ClavierOperation
    //le paramètre est l'id du div dans la page
    function ClavierChiffres(idClavier) {
        this.idClavier = idClavier; //mise en place du parametre
        //this.outputTest = null; //pour test

        this.init = function () { //fonction compatible init
            calculette.logger.log("ClavierChiffres","Init instance ClavierChiffres");
            var racineClavier = $("#" + this.idClavier);

            //pour test (debut)
            //this.outputTest = $('<div>');
            //racineClavier.append(this.outputTest);
            //pour test (fin)

            for (var cpt = 1; cpt <= 9; cpt++) {
                var newElement = $('<button>'); //creation element
                newElement.text(cpt.toString()); //mise en place libelle
                newElement.addClass('btn btn-outline-primary col-3');
                newElement.data('idbt', cpt);
                newElement.on('click', function () {
                    var elemntClick = $(this);
                    calculette.logger.log("ClavierChiffres","Click " + elemntClick.data('idbt'));
                    try {
                        calculette.controler.addChiffre(elemntClick.data('idbt'));
                    }
                    catch (ex) {
                        //alert(ex);
                    }
                });
                racineClavier.append(newElement);
            }
            var cpt = 0;
            var newElement = $('<button>'); //creation element
                newElement.text(cpt.toString()); //mise en place libelle
                newElement.addClass('btn btn-outline-primary col-10');
                newElement.data('idbt', cpt);
                racineClavier.append(newElement);
                newElement.on('click', function () {
                  var elemntClick = $(this);
                  calculette.logger.log("ClavierChiffres","Click " + elemntClick.data('idbt'));
                
                try {
                  calculette.controler.addChiffre(elemntClick.data('idbt'));
              }
              catch (ex) {
                  //alert(ex);
              }
            });
        };

        //Utilisation du clavier numérique
        document.addEventListener('keydown', (event) => {
            if (Number.isInteger(Number(event.key))) {
                calculette.logger.log("ClavierChiffres","Appuie sur la touche " + event.key);
                try {
                    calculette.controler.addChiffre(event.key);
                }
                catch (ex) {
                    //alert(ex);
                }
            }
           
        });

        // TODO
        //uniquement pour la mis au point du composant
        //a supprimer pour la version finale
        //!!!!! important
        this.refreshView = function () {
            //this.outputTest.text(calculette.model.saisieVal); //affichage
        };

    }
    var clavier = new ClavierChiffres('clvdigit1'); //creation d'une instance de type ClavierOperation
    //abonnement de l'instance au moteur application
    //window.calculette
    calculette.aboInitApp(clavier);

})();