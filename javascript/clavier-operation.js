(function(){
    //test si calculette installée dans la page
    if (!('calculette' in window)){
        calculette.logger.error("ClavierOperation","Problème pas de calculette");
        return; //sortie de la fonction
    }

    //creation du type (class) ClavierOperation
    //le paramètre est l'id du div dans la page
    function ClavierOperation(idClavier){
        this.idClavier = idClavier; //mise en place du parametre
        this.init = function(){
            calculette.logger.log("ClavierOperation","Init instance ClavierOperation");
            //recherche du BT plus
            var bcl1 = `#${this.idClavier} button.cl_plus`;
            $("#"+this.idClavier+" button.cl_plus").on('click',function(){
                calculette.logger.log("ClavierOperation",'click plus');
                //recuperation valeur saisie dans le model
                var valSaisie = parseInt(calculette.model.saisieVal,10);
                if (isNaN(valSaisie)){
                    valSaisie = 0;
                    calculette.model.saisieVal="";
                }
                //reset zone
                calculette.model.saisieVal="";
                calculette.controler.plus(valSaisie);
                
            });
            //recherche du BT moins
            $("#"+this.idClavier+" button.cl_moins").on('click',function(){
                calculette.logger.log("ClavierOperation",'click moins');
                var valSaisie = parseInt(calculette.model.saisieVal,10);
                if (isNaN(valSaisie)){
                    valSaisie = 0;
                    calculette.model.saisieVal="";
                }
                //reset zone
                calculette.model.saisieVal="";
                calculette.controler.moins(valSaisie);
            });
            //recherche du BT reset
            $("#"+this.idClavier+" button.cl_reset").on('click',function(){
                calculette.logger.log("ClavierOperation",'click reset');
                //reset zone de saisie
                calculette.model.saisieVal=""
                calculette.controler.reset();    
            });
            //recherche du BT test
            $("#"+this.idClavier+" button.cl_worktotal").on('click',function(){
                calculette.logger.log("ClavierOperation",'click reset');
                calculette.controler.processTotal4Log();    
            });

            //Utilisation du clavier numérique
            document.addEventListener('keydown', (event) => {
                if (event.key === '+') {
                        calculette.logger.log("ClavierOperation",'appuie touche plus');
                        var valSaisie = parseInt(calculette.model.saisieVal,10);
                        if (isNaN(valSaisie)){
                            valSaisie = 0;
                            calculette.model.saisieVal="";
                        }
                        //reset zone
                        calculette.model.saisieVal="";
                        calculette.controler.plus(valSaisie);
                    } else if (event.key === '-') {
                        calculette.logger.log("ClavierOperation",'appuie touche moins');
                        var valSaisie = parseInt(calculette.model.saisieVal,10);
                        if (isNaN(valSaisie)){
                            valSaisie = 0;
                            calculette.model.saisieVal="";
                        }
                        //reset zone
                        calculette.model.saisieVal="";
                        calculette.controler.moins(valSaisie);
                    }
           
            });


            
        };
    }

    var clavier = new ClavierOperation('clvact1'); //creation d'une instance de type ClavierOperation
    //abonnement de l'instance au moteur application
    //window.calculette
    calculette.aboInitApp(clavier); 

})();